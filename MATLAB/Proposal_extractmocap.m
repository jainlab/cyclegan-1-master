S = load('C:\Users\CISE\Documents\GitHub\cyclegan-1-master\code and data\style_motion_database');
modata = S.motion_database;
windowsize = 24;%30
% multiply of 4
overlap = 8;%24
motion_counter = 1;
prev_motionName = 'uu';
 foldernames = {'childlike','old'};
 count =0;
 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
 for i =1:2
     if i == 1
         csvname_max = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\input\adult2child\childlike\childlike_max.csv';
         csvname_min = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\input\adult2child\childlike\childlike_min.csv';
     else
         csvname_max = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\input\adult2child\old\old_max.csv';
        csvname_min = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\input\adult2child\old\old_min.csv';

     end
     max_final = csvread( csvname_max);
    min_final = csvread( csvname_min);

     
     foldername = foldernames(i);
for iter = 1:572
  
 
   if strcmp(modata(iter).styleName ,  foldername)
   
       motionName = strrep(modata(iter).contentName,' ','');
    
       
       if ~strcmp(motionName, prev_motionName)
        filename = strcat ('C:\Users\CISE\Documents\GitHub\cyclegan-1-master\input\adult2child\',foldername, '_test\',  foldername,'_' ,  motionName, '_' ,string( motion_counter),'_' );
        %filename = strcat (modata(iter).styleName, '_' , motionName ,string(iter),'_' );
       
        %fid = fopen(filename','wt');
        motion_counter = 0;
       else
         motion_counter = motion_counter + 1;
         filename = strcat ('C:\Users\CISE\Documents\GitHub\cyclegan-1-master\input\adult2child\',foldername, '\', foldername,'_' ,  motionName,'_' , string( motion_counter), '_' );   
         %filename = strcat (modata(iter).styleName, '_' , motionName ,string(iter),'_' );
       
       end
        prev_motionName = motionName;
       
        
        frame = 1;
     
        root_rotation = modata(iter).motion(frame).rotation(1,:);
       rotation_combined = decomposeRotation(root_rotation); 
        
        
      frame_num = size(modata(iter).motion);
      
      for motion_clip = 1: floor((frame_num(2)-overlap)/(windowsize-overlap))
        
         if motion_clip<10
             
             str_clip_num  = strcat('0', string(motion_clip));
         else
             str_clip_num  =string(motion_clip);
             
         end
        csvname = strcat (filename,str_clip_num,'.csv');
      
        A= [];
        frame_csv = 1;
        for frame = (motion_clip-1)*(windowsize-overlap)+1:(motion_clip-1)*(windowsize-overlap) +windowsize
             A(frame_csv, 1:3)= modata(iter).motion(frame).position ;  

             A(frame_csv, 4:3+25*4) = reshape(modata(iter).motion(frame).rotation.', 1, 25*4);
            % disp( A(frame_csv, 4:7))
            A(frame_csv, 4:7) =   quatmultiply_mutate( rotation_combined, A(frame_csv, 4:7));
             % disp( A(frame_csv, 4:7))
              % A(frame_csv, 4:7) =  [0,0,0,1];
             frame_csv = frame_csv + 1;
             
        end

         

           A(:, [ 8:11, 32:35 ,52:55, 60:63, 80:83, 100:103])=[];
           A = [zeros(windowsize,1) A];
         
          C =  bsxfun(@rdivide, 2*A- min_final - max_final, max_final-min_final);
            C(isnan(C)) = 0;
          
          
          
          
          A_reshape = reshape(C.', 1, windowsize*80);
          csvwrite( csvname, A_reshape);
          
         
          
      end


   end
       
end
 end
path = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\horse2zebra\trainA';
file = dir (fullfile(path,'*.jpg'));

L = length (file);

%name = 'childlike_fast_walking152_1_fake_epoch102.csv';

for file_num=1:L
   % process the image in here
position = [];
name =  file(file_num).name;

jpgname=strcat(path,'\', name);

img = imread(jpgname);

 csvname = strrep(jpgname, 'jpg', 'csv');
  %img(:,:,1)= (img(:,:,1)+ img(:,:,2)+ img(:,:,3))/3;
  J = imresize( img(:,:,1), 0.5);
img_reshape = reshape(J, 1,256*256/4);
 csvwrite( csvname,img_reshape);

end
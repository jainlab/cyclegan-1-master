%take bvh file and convert to joint position from joint rotation
path = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old';
file = dir (fullfile(path,'*.bvh'));
windowsize = 10;
L = length (file);

%name = 'childlike_fast_walking152_1_fake_epoch102.csv';

for file_num=2:2
   % process the image in here
position = [];
name =  file(file_num).name;

bvhname=strcat(path,'\', name);

[skeleton,time,fps,fname] = loadbvh_andreas(bvhname);

for joint = 1:25
if skeleton(joint).name ==' '
    continue
end
position = [position;skeleton(joint).Dxyz];


end

positionT = position.'

     positionT_reshape = reshape(position, 1, windowsize*60);
     
     csvname = strrep(bvhname, 'bvh', 'csv');
    csvwrite( csvname,positionT_reshape);


end
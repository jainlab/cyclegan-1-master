
class = ["old", "childlike"];
image_type = ["fake", "input", "cycle"];

motion = ["fast_puching", "fast_walking", "jumping", "kicking", "normal_walking", "running", "transition" ];
epoch = 199;

% for epoch = 1:n
%     
%     for class
%     
%         for type in image_type 
%         
%             for 
%         
%     
%             end

            
[num,txt,raw]=xlsread('C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\adult2child_train_flatten_Copy.xlsx');

 for col = 1: size(raw,2)      
  for row = 1: size(raw,1)      
     for motion_num = 1:length(motion)
         
        %./CycleGAN_TensorFlow/input/adult2child/childlike/childlike_fast_punching439_1.csv
         if ~(isempty(strfind(raw{row,col}, motion(motion_num))));
             filename_split0 = split(raw{row,col},"/");
             filename_split1 = split(filename_split0(length(filename_split0)),motion(motion_num));
             filename_split2 = split( filename_split1(2), '_');
             filename_split3 =  filename_split2(1);
             
         else
          continue
         end    
     
   
         %"_" , int2str(clip),"_", "fake_epoch",int2str(epoch),".csv"
            mocap = [];
            %childlike_fast_punching439_1_cycle_epoch58
            for clip = 1:20
                filename = strcat (filename_split1(1), motion(motion_num), filename_split3, "_", int2str(clip),".csv");
                    file_exist = contains(raw,filename);
                    if not(all(file_exist(:) < 0.5))
                        
                        for epoch = 1:200
                            full_path = strcat("C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\output\cyclegan\exp_01\20190228-191349\imgs\",filename_split1(1), motion(motion_num), filename_split3, "_", int2str(clip),"_" , "fake_epoch",int2str(epoch),".csv");
                            if exist(full_path, 'file') == 2 
                        
                            [num,txt,raw_new]=xlsread(full_path);
                             mocap = [mocap ; raw_new] ;
                             
                             
                           
                            end
                
                        end
                    else
                        if(clip>0)
                        csvwrite (strcat("C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\output\cyclegan\exp_01\20190228-191349\img_integrate\",filename_split1(1), motion(motion_num), filename_split3, "_", "fake_epoch",int2str(epoch),".csv" ),mocap) ;
                        end
                        break
                end
    
         end 
      end
    end    
  end

%path = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\output\cyclegan\exp_01\20200128-152051\imgs\concatenated\smoothed\';
% path = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\output\cyclegan\exp_01\20200128-152051\imgs\';
% %path = 'C:\Users\CISE\Documents\GitHub\hpgan\src\results\output\videos\20200202-000938\concatenated\'
% %path = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\concatenated\'
% path = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\concatenated_nooverlap';
% path = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\output\cyclegan\exp_01\20200128-152051\imgs\concatenated_nooverlap\';
% path = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\output\cyclegan\exp_01\20200128-152051\imgs\smoothed\concatenated';
% 
% path = 'C:\Users\CISE\Documents\GitHub\hpgan\src\results\output\videos\20200219-073522\concatenated\';
% 
% path = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\output\cyclegan\exp_01\20200128-152051\imgs\smoothed\concatenated_nooverlap\';
% path = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\'
path = 'C:\Users\CISE\Documents\GitHub\cyclegan-1-master\output\cyclegan\exp_01\20200304-143654\imgs\concatenated';

file = dir (fullfile(path,'*.csv'));

L = length (file);

%name = 'childlike_fast_walking152_1_fake_epoch102.csv';

% 



for file_num=1:L
   % process the image in here

   
   
   %name = 'old_fastwalking_5_fake_epoch117.csv';
name =  file(file_num).name;

TF = contains(name,'old');
TF1 = contains(name,'childlike');
TF2 = contains(name,'fake');
TF3 = contains(name,'input');
if contains(name,'cycle')
    continue
end

if contains(name,'childlike')
    continue
end

if TF && TF2 
csvname_max = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\childlike\childlike_max.csv';
csvname_min = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\childlike\childlike_min.csv';
end


if TF && TF3
csvname_max = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\old_max.csv';
csvname_min = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\old_min.csv';
end

if TF1 && TF3 
csvname_max = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\childlike\childlike_max.csv';
csvname_min = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\childlike\childlike_min.csv';
end


if TF1 && TF2
csvname_max = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\old_max.csv';
csvname_min = 'C:\Users\CISE\Documents\GitHub\CycleGAN_TensorFlow\input\adult2child\old\old_min.csv';
end

max_final = csvread( csvname_max);
min_final = csvread( csvname_min);




%name =  'old_fastwalking_1.csv';
%name =  'childlike_normalwalking_2_12_fake_epoch149.csv';
csv_read = csvread(strcat(path,'\', name));
csvname=strcat(path,'\', name);

%csv = csvread(strcat(path,'\',file(file_num).name));


windowsize = size(csv_read,1);
 %windowsize = 24;
%   csv = reshape(csv_read, 80, 24);
%  csv = csv.';
 csv =csv_read;
 csv =( bsxfun(@times,(max_final -min_final ),csv) + min_final+ max_final )/2;
 
 
A = [];

 B= [zeros(windowsize,1) zeros(windowsize,1) zeros(windowsize,1) ones(windowsize,1)];

  skip = [8:11, 32:35, 52:55, 60:63, 80:83, 100:103];
 skip = reshape(skip,[4,6]).';
%csv = [zeros(windowsize,1) zeros(windowsize,1) zeros(windowsize,1) csv];
%csv(:,1:6) = [zeros(windowsize,1) zeros(windowsize,1) zeros(windowsize,1) zeros(windowsize,1) zeros(windowsize,1) zeros(windowsize,1)];
csv(:,1) = [];

 for i = 1:size (skip,1)
  insert = skip (i,1)-1   ;
 csv = [csv(:,1 :insert) B csv( :,insert+1:end)]  ;
 
 end
       %fid = fopen(filename','wt');
       
       
      frame_num = size(csv);

      % csv(:,4:6) = [zeros(frame_num(1),1),zeros(frame_num(1),1),ones(frame_num(1),1)];
      
      for frame = 1:frame_num(1)
          A(frame, 1:3)= csv (frame, 1:3);  
          quat_mat1 = csv (frame, 4:end);
           
          quat_mat =  reshape(quat_mat1,[4,25]).';
          col_num = 0;
           for joint = 1:25 
              
              if joint == 8 |   joint ==13 | joint ==15 | joint ==20  | joint == 25     
                  continue
              end
           col_num = col_num+1;
    
           euler = quatern2euler([quat_mat(joint,4),quat_mat(joint,1),quat_mat(joint,2),quat_mat(joint,3)]);
           A(frame, 4+ 3*( col_num-1): 6+ 3*( col_num-1)) = rad2deg([euler(3),euler(2),euler(1)]);
            
            
           end
         
      end
    
% Prepend the folder:
      %imwrite(A, fullFileName );
       % fprintf(fid, '%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s\n', New{1,:});
       % for i=2:size(New,1)
       % fprintf(fid, '%d,%s,%d,%s,%d,%d,%s,%s,%d,%d,%d\n', New{i,:});
       % end
        %fclose(fid);
      % Create the base filename for the PNG file.
 copyfile('C:\Users\CISE\Desktop\basic_xia.bvh');
 fid=fopen('basic_xia.bvh','a');
%fprintf(fid,'\n');

fprintf(fid,'\nFrames: %g\r\n',frame_num(1));
fprintf(fid,'Frame Time: .0333333\r\n');
for i=1:frame_num(1)
    for j=1:63
    fprintf(fid,'%g ',A(i,j));
    end
    fprintf(fid,'\n');
end
fclose(fid);

bvh_name = strrep(csvname, 'csv', 'bvh');
%bvh_name = strrep(file(file_num).name, 'csv', 'bvh');
movefile('basic_xia.bvh', bvh_name);


end
       

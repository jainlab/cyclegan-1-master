function    result = decomposeRotation(root_rotation)
 root_rotation = [root_rotation(:,4) root_rotation(:,1) root_rotation(:,2) root_rotation(:,3)];
Y_axis_of_global_coordinates = [0, 1, 0];
 z_axis_of_global_coordinates = [0, 0, 1];
    %compute the Y-axis of the root's local coordinates
    Y_axis_of_root_coordinates = quatRot(Y_axis_of_global_coordinates ,root_rotation);
 Y_axis_of_root_coordinates_unit = quatnormalize (Y_axis_of_root_coordinates);
 
     %compute the z-axis of the root's local coordinates
    z_axis_of_root_coordinates = quatRot(z_axis_of_global_coordinates ,root_rotation);
 z_axis_of_root_coordinates_unit = quatnormalize (z_axis_of_root_coordinates);
 
 
    % Next step is to rotate the local coordinates so that the local Y-axis coincides with global Y-axis.
    % After this rotation, the return value only contains rotation along global Y-axis, which could be used to compute facing direction.
    rotAxis = cross(Y_axis_of_root_coordinates_unit, Y_axis_of_global_coordinates);
 
   
   %normalize vector
    if (sqrt(rotAxis' * rotAxis) <0.0001)
         rotAxis_unit = rotAxis ;
    else
        rotAxis_unit = rotAxis /norm(rotAxis);
    end

    rot_cos = dot(Y_axis_of_root_coordinates_unit, Y_axis_of_global_coordinates);

    if (rot_cos  > 1)
        rotAngle_axis = pi;
    elseif (rot_cos  < -1)
        rotAngle_axis = 0;
    else
        rotAngle_axis = acos(rot_cos);
    end
    rot_axis = [cos(rotAngle_axis/2.), rotAxis_unit*sin(rotAngle_axis/2.)];% this is a rotation perpendicular to Y-axis

    
   rot_cos_y = dot(z_axis_of_root_coordinates_unit, z_axis_of_global_coordinates);

if (z_axis_of_root_coordinates_unit(1)>= 0)

   rotAngle_y = 0;
 else 
 rotAngle_y= pi;

end
%    disp( rotAngle_axis );
%    disp(rotAngle_axis);
%     disp( Y_axis_of_root_coordinates );
    rot_y = [cos(rotAngle_y/2.), Y_axis_of_global_coordinates*sin(rotAngle_y/2.)];% this is a rotation perpendicular to Y-axis
    disp( z_axis_of_root_coordinates_unit)
    
    
    
    rotation_combined = quatmultiply(  quaternConj(rot_axis),  quatmultiply(rot_y, rot_axis)); % this is a rotation along Y-axis
 %  rotation_combined = rot_y ; % this is a rotation along Y-axis
%     result_tmp = rot;
     result = [rotation_combined(:,2) rotation_combined(:,3) rotation_combined(:,4) rotation_combined(:,1)];
   


end
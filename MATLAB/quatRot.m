%rotate a vector by quaternion

function result = quatRot(vector, q)


 qConj = quaternConj(q);

 quat_v = [0, vector];
 
 tmp = quatmultiply(quat_v, qConj);

 mult = quatmultiply( q,tmp);

result = mult(2:end);
 

end
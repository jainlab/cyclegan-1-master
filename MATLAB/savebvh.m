%
% $Id: savebvh.m 4802 2015-12-11 19:45:02Z stathis $
%
% BVH Exporter for MATLAB (stathis@npcglib.org)
%
% savebvh(skeleton,time,fname)
%
% To use this do:
%      
%    skeleton = savebvh(skeleton, time, 'output.bvh');
%
%    skeleton: the structure of the skeleton and 
%              animation data as defined by loadbvh
%        time: the structure of frame times as defined by loadbvh
%       fname: the filename to use for saving the file 
%              (existing files are truncated)
%
function skeleton = savebvh(skeleton,time,fname)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% extract children for each joint
for idx = 1:numel(skeleton)
    skeleton(idx).Nchildren = [];
    
    for chID = 1:numel(skeleton)
        child = skeleton(chID);
        
        if ( child.parent == idx )
            skeleton(idx).Nchildren = [skeleton(idx).Nchildren chID];
        end
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

fileID = fopen(fname,'w');

% skeleton structure
%fprintf(fileID,'HIERARCHY');
for idx = 1:numel(skeleton)
    element = skeleton(idx);
    %saveJoint(idx, skeleton, element, fileID);
    break;
end

% motion data
% fprintf(fileID,'\nMOTION');
% fprintf(fileID,'\nFrames: %d', numel(time));
% fprintf(fileID,'\nFrame Time: %g\n', time(2));
for tID = 1:numel(time)
    for idx = 1:numel(skeleton)
        element = skeleton(idx);
        saveMotion(tID, idx, skeleton, element, fileID);
        %fprintf(fileID,'\n');
        break
    end
    %break; % remove this
end
%fprintf(fileID,'\n');
fclose(fileID);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function saveJoint(id, skeleton, element, fileID)

idx = id;
tabs = '';

for f = 2:element.nestdepth
   new_tabs = sprintf('  %s',tabs);
   tabs = new_tabs;
end

shift_tabs = sprintf('  %s',tabs);

if element.nestdepth == 1
    jointType = 'ROOT';
elseif element.name == ' '
    jointType = 'End Site';
else
    jointType = 'JOINT';
end

fprintf(fileID,'\n%s%s %s\n%s{', tabs, jointType, element.name, tabs);

fprintf(fileID,'\n%sOFFSET %.4f %.4f %.4f', shift_tabs, ...
                                      element.offset(1,:), ...
                                      element.offset(2,:), ...
                                      element.offset(3,:));

if ( not (element.Nchannels == 0) )
    
    rotOrder = {};
    for i = element.order
        if ( i == 1 )
            rotOrder = [rotOrder 'Xposition'];
        elseif ( i == 2 )
            rotOrder = [rotOrder 'Yposition'];
        elseif ( i == 3 )
            rotOrder = [rotOrder 'Zposition'];
        end
    end
    
    if element.Nchannels == 6
        fprintf(fileID,'\n%sCHANNELS %d Xposition Yposition Zposition %s %s %s',shift_tabs, ...
                        element.Nchannels, ...
                        char(rotOrder(1)), ...
                        char(rotOrder(2)), ...
                        char(rotOrder(3)));
    elseif element.Nchannels == 3
        fprintf(fileID,'\n%sCHANNELS %d %s %s %s',shift_tabs, ...
                    element.Nchannels, ...
                    char(rotOrder(1)), ...
                    char(rotOrder(2)), ...
                    char(rotOrder(3)));
    end
end

for i = 1:numel(element.Nchildren)
   child = skeleton(element.Nchildren(i));
   fprintf(fileID,'%s', tabs);
   saveJoint(element.Nchildren(i), skeleton, child, fileID)
end

fprintf(fileID,'\n%s}', tabs);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function saveMotion(timeID, id, skeleton, element, fileID)
matrix = [];
idx = id;

if element.nestdepth == 1
    jointType = 'ROOT';
elseif element.name == ' '
    jointType = 'End Site';
else
    jointType = 'JOINT';
end

% translation
offX = element.offset(1,1);
offY = element.offset(2,1);
offZ = element.offset(3,1);

if ( strcmp(jointType,'ROOT') )
    dx = element.Dxyz(1,timeID) - offX;
    dy = element.Dxyz(2,timeID) - offY;
    dz = element.Dxyz(3,timeID) - offZ;

    fprintf(fileID,'%.4f, %.4f, %.4f,', dx, dy, dz);
end

% -------------------------------------------------------------------------
% translation
% -------------------------------------------------------------------------
if ( strcmp(jointType,'JOINT') )
    parent = skeleton(element.parent);
    
    dx = element.Dxyz(1,timeID) - parent.Dxyz(1,timeID);
    dy = element.Dxyz(2,timeID) - parent.Dxyz(2,timeID);
    dz = element.Dxyz(3,timeID) - parent.Dxyz(3,timeID);
    matrix = [matrix, dx, dy, dz];

    fprintf(fileID,'%g, %g, %g,', dx, dy, dz);
end

if ( strcmp(jointType,'JOINT') )
   %fprintf(fileID,'%g %g %g ', offX, offY, offZ);
end

% -------------------------------------------------------------------------
% rotation
% -------------------------------------------------------------------------
% if ( not (element.Nchannels == 0) )
%     rotOrder = [];
%     for i = element.order
%         rotOrder = [rotOrder element.rxyz(i,timeID)];
%     end
%     
%     fprintf(fileID,'%.4f %.4f %.4f ', rotOrder(1), rotOrder(2), rotOrder(3));
% %     fprintf(fileID,'0 0 0 ');
% end

for i = 1:numel(element.Nchildren)
   child = skeleton(element.Nchildren(i));
   saveMotion(timeID, element.Nchildren(i), skeleton, child, fileID)
end

end


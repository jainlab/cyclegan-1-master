
[The most up-to-date version is coming soon]

# CycleGAN for motion stylization


This is the TensorFlow implementation for CycleGAN for motion stylization. The code was written by [Harry Yang](https://www.harryyang.org) and [Nathan Silberman](https://github.com/nathansilberman) and adapted by Yuzhu Dong(yuzhudo1g@ufl.edu)


## Introduction

This code contains both MATLAB scripts and python scripts. 


## Getting Started
### Prepare dataset

The motion capture dataset is from Xia et al[2015] and Dong et al[2020]. For Xia et al.[2015], the data can be found in "code and data.zip". 
To comute the min and max values for each DOF across all the motions, run MATLAB/findminmax.m. min and max values would be stored in the "cyclegan-1-master\input\adult2child\old\" directory

To export data from a .mat file to csv files, run code MATLAB/Proposal_extractmocap.m

Create another csv file adult2child.csv as input to the data loader with same motion type paired. This needs to run in Ubuntu
>>cd ../mnt/c/Users/CISE/Documents/GitHub
>>python3 -m cyclegan-1-master.match_input_pairs

To attach values to the same csv files. We do so to avoid TF read the adult2child.csv file and the actual motion files back to back. It would mess up the reading input thread 
>>python3 -m cyclegan-1-master.add_value_cyclegan_dataset

### Training
* Create the configuration file. The configuration file contains basic information for training/testing. An example of the configuration file could be fond at configs/exp_01.json. 

* Start training:
In anaconda promp:
>>activate tensorflow
>>cd Documents\GitHub\
>>python -m cyclegan-1-master.main  --to_train=1  --log_dir=cyclegan-1-master/output/cyclegan/exp_01 --config_filename=cyclegan-1-master/configs/exp_01.json --skip=True

* Check the intermediate results.
>>tensorboard --port=6006 --logdir=cyclegan-1-master/output/cyclegan/exp_01/20200126-161245

### Restoring from the previous checkpoint.
>>python -m cyclegan-1-master.main   --to_train=2  --log_dir=cyclegan-1-master/output/cyclegan/exp_01  --config_filename=cyclegan-1-master/configs/exp_01_test.json   --checkpoint_dir=cyclegan-1-master/output/cyclegan/exp_01/20200126-161245

### Testing
>>python -m cyclegan-1-master.main  --to_train=0  --log_dir=cyclegan-1-master/output/cyclegan/exp_01 --config_filename=cyclegan-1-master/configs/exp_01.json --checkpoint_dir=cyclegan-1-master/output/cyclegan/exp_01/20200126-161245


#### Apply low pass filter
>>python -m  cyclegan-1-master.lowpass  --dir_name=./cyclegan-1-master/output/cyclegan/exp_01/20200223-203832/imgs/

### concatenate motion words into original sequences
python3 -m cyclegan-1-master.concatenate --image_path_a=./cyclegan-1-master/output/cyclegan/exp_01/20200128-152051/imgs/ --output_dir=./cyclegan-1-master/output/cyclegan/exp_01/20200223-203832/imgs/concatenated/



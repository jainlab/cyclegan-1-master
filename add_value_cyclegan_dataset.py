"""Add values to the input csv files"""
import csv
import os
import random
import numpy as np
import click
import os
from . import cyclegan_datasets


def create_list(foldername, fulldir=True, suffix=".csv"):
    """

    :param foldername: The full path of the folder.
    :param fulldir: Whether to return the full path or not.
    :param suffix: Filter by suffix.

    :return: The list of filenames in the folder with given suffix.

    """
    file_list_tmp = os.listdir(foldername)
    file_list_tmp.sort()
    file_list = []
    if fulldir:
        for item in file_list_tmp:
            if item.endswith(suffix):
                file_list.append(os.path.join(foldername, item))
    else:
        for item in file_list_tmp:
            if item.endswith(suffix):
                file_list.append(item)
    return file_list




    
            
def create_dataset(image_path_a, image_path_b,
                   dataset_name, do_shuffle):
    list_a = create_list(image_path_a, True,
                         cyclegan_datasets.DATASET_TO_IMAGETYPE[dataset_name])
    list_b = create_list(image_path_b, True,
                         cyclegan_datasets.DATASET_TO_IMAGETYPE[dataset_name])

    output_path = cyclegan_datasets.PATH_TO_CSV[dataset_name]

    num_rows = cyclegan_datasets.DATASET_TO_SIZES[dataset_name]
    all_data_tuples = []
    
    
 
    
    for i in range(num_rows):
        
        all_data_tuples.append((
            list_a[i % len(list_a)],
            list_b[i % len(list_b)]
        ))
    if do_shuffle is True:
        random.shuffle(all_data_tuples)
    with open(output_path, 'w') as csv_file:
        csv_writer = csv.writer(csv_file)
        for data_tuple in enumerate(all_data_tuples):
            csv_writer.writerow(list(data_tuple[1]))

            
@click.command()
@click.option('--image_path_a',
              type=click.STRING,
              default='./cyclegan-1-master/input/adult2child/old',
              help='The path to the images from domain_a.')
@click.option('--image_path_b',
              type=click.STRING,
              default='./cyclegan-1-master/input/adult2child/childlike',
              help='The path to the images from domain_b.')
@click.option('--dataset_name',
              type=click.STRING,
              default='adult2child_train',
              help='The name of the dataset in cyclegan_dataset.')
@click.option('--dataset_name_output',
              type=click.STRING,
              default='adult2child_train_values',
              help='The name of the dataset in cyclegan_dataset.')
@click.option('--do_shuffle',
              type=click.BOOL,
              default=False,
              help='Whether to shuffle images when creating the dataset.')      
def addvalue_dataset(image_path_a, image_path_b,
                   dataset_name, dataset_name_output, do_shuffle):
    
    rows = []    
    count = 0
        
    input_path = cyclegan_datasets.PATH_TO_CSV[dataset_name]
    with open(input_path, 'r') as csvfile: 
    # creating a csv reader object 
        csvreader_filename = csv.reader(csvfile) 
   
        # extracting each data row one by one 
        for row in csvreader_filename: 
            count = count + 1
            for col in row:
                with open(col, 'r') as csvfile: 
                    # creating a csv reader object 
                    csvreader_value = csv.reader(csvfile) 
      
                    # extracting field names through first row 
                    value = next(csvreader_value)
                    value.insert(0,col)
                    rows.extend(value)
                

                 
        array = np.array(rows) 
        #cell_nums = model.IMG_WIDTH *model.IMG_HEIGHT*model.IMG_CHANNELS +1
        new_rows = np.array(rows).reshape(count, 1921*2)
#    
#    
    output_path = cyclegan_datasets.PATH_TO_CSV[dataset_name_output]
    with open(output_path, 'w' ) as csv_file:
        csv_writer = csv.writer(csv_file)
        for data_tuple in new_rows:
            csv_writer.writerow(data_tuple)

    os.rename(output_path,input_path)
            
if __name__ == '__main__':
    addvalue_dataset()

"""
This script serves to extract frame motions in all degrees of freedom (DOF) 
from .bvh files and export those into .csv files with headers. The .csv file(s)
maintain the same name as the .bvh(s). In a .csv, each column corresponds to
one DOF (many columns per joint) and each row corresponds to a frame. Joints 
retain the same ordering.
"""

import sys
import os

BVH_PATH =  r'D:\mocap_adult2child\bvh_3dof\\'
OUTPUT_FOLDER = r'D:\mocap_adult2child\csv\\'

def writeToCvs(bvh, csv):
    for line in bvh_iterator(bvh, csv):
        csv.write(','.join(line.split()))
        csv.write('\n')

def bvh_iterator(bvh, csv):
    header = []

    for line in bvh: # Go through beginning hierarchy info -> need to write headers
        if 'ROOT' in line or 'JOINT' in line: #
            currJoint = line.split()[1] # line format ROOT/JOINT jointName

        if 'CHANNELS' in line:
            chanList = line.split()[2:] # line format CHANNELS chanCount [dof]

            header += [('_'.join([currJoint, c[1:4], c[0]])) for c in chanList]

        if 'Frame Time:' in line: # Next lines are motion data -> break
            break

    csv.write(','.join(header) + '\n') # Write header

    for line in bvh: # For each frame line
        yield line
         
def iterateDir(root):
    outputSub = None
    
    # Make output dirs
    os.makedirs(OUTPUT_FOLDER, exist_ok=True)
    os.makedirs(OUTPUT_FOLDER + 'adult\\', exist_ok=True)
    os.makedirs(OUTPUT_FOLDER + 'child\\', exist_ok=True)
    
    for path, dirs, files in os.walk(root): 
        output_sub = 'adult\\' if '\\adult' in path.lower() else 'child\\'
        bvh_files = [f for f in files if ('.bvh' in f)] # only care about bvh files
       
        # Loop through all bvh files and create csv files
        for bvh_file in  bvh_files:
            bvh_name, bvh_ext = os.path.splitext(bvh_file)
           
            bvh_path =  path + "\\" + bvh_file
            csv_path = OUTPUT_FOLDER + output_sub + bvh_name + '.csv'
            bvh = open(bvh_path, "r") # Will throw error if file DNE
            csv = open(csv_path, "w") # Will throw error if file DNE
            
            writeToCvs(bvh, csv)
          
            bvh.close()
            csv.close()                   
                   
if __name__ == '__main__':
    iterateDir(BVH_PATH)

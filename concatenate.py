
import csv
import os
import random
import numpy as np
import click
from numpy import genfromtxt

def create_list(foldername, fulldir=True, suffix=".csv"):
    """

    :param foldername: The full path of the folder.
    :param fulldir: Whether to return the full path or not.
    :param suffix: Filter by suffix.

    :return: The list of filenames in the folder with given suffix.

    """
    file_list_tmp = os.listdir(foldername)
   
    file_list_tmp.sort()
    file_list = []

    if fulldir:
        for item in file_list_tmp:
            if item.endswith(suffix):
                file_list.append(os.path.join(foldername, item))
    else:
        for item in file_list_tmp :
            if item.endswith(suffix):
                file_list.append(item)
    return file_list




#'./cyclegan-1-master/output/cyclegan/exp_01/20200107-154152/imgs/

@click.command()
@click.option('--image_path_a',
              type=click.STRING,
              default='./CycleGAN_TensorFlow/input/adult2child/old/',
              help='The path to the images from domain_a.')

@click.option('--output_dir',
              type=click.STRING,
              default='./CycleGAN_TensorFlow/input/adult2child/old/integrate/',
              help='The path to the images from domain_a.')

def concatenate(image_path_a, output_dir):
    list_a = create_list(image_path_a, True,
                         '.csv')
    
    IMG_HEIGHT = 20

    # The width of each image.
    IMG_WIDTH = 24

    # The number of color channels per image.
    IMG_CHANNELS = 4
    motions  = ['kick','fastwalking', 'jumping','kicking','normalwalking','punching','running','transitions' ]

    dict_file = {}
    styles = ['old']
    types = ['_input', '_fake']
    overlap = 4
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    

    for style in styles:   
        for motion in motions:
            for type_motion in types:
          
                sequence = 1
            
                acces = type_motion+'_epoch170'
            
            
            
                dict_file[style+'_'+motion] =  [i for i in list_a if style+'_'+motion in i] 
                print(image_path_a + style+'_'+motion+'_'+ str(sequence) + '_' + '01' + '.csv')         
                while image_path_a + style+'_'+motion+'_'+ str(sequence) + '_' +'01'  + acces+ '.csv' in  dict_file[style+'_'+motion]:
                    clip = 2
                    clip_str = '02'
                    output_path =  output_dir + style+'_'+motion+'_'+ str(sequence)+ acces +'.csv'
                
                    if not os.path.exists( output_dir):
                        os.makedirs( output_dir)
                    print( output_dir)
             
                    my_data1 =[]
                
                    my_data1 = genfromtxt(image_path_a +style+'_'+motion+'_'+ str(sequence)+'_'+'01'+ acces+'.csv', delimiter=',')    
                    my_data1 = np.reshape(my_data1, (IMG_WIDTH,IMG_HEIGHT*IMG_CHANNELS))
                    print(my_data1.size)
                
                    while  image_path_a+ style+'_'+motion+'_'+ str(sequence)+'_'+clip_str+ acces+'.csv' in dict_file[style+'_'+motion]:
        
                       
                        print( image_path_a+ style+'_'+motion+'_'+ str(sequence)+'_'+clip_str + acces+'.csv')
                
                        my_data2 = []
                        my_data2 = genfromtxt(image_path_a +style+'_'+motion+'_'+ str(sequence)+'_'+clip_str + acces +'.csv', delimiter=',')
                        my_data2 = np.reshape(my_data2, (IMG_WIDTH,IMG_HEIGHT*IMG_CHANNELS))
#                       r = csv.reader(open(image_path_a +style+'_'+motion+'_'+ str(sequence)+'_'+str(clip)  + '_input_epoch0'+'.csv')) # Here your csv file
#                       my_data2 = list(r) 
#                       r = csv.reader(open(image_path_a +style+'_'+motion+'_'+ str(sequence)+'_'+str(clip-1)  + '_input_epoch0'+'.csv')) # Here your csv file
#                       my_data1 = list(r) 
                        my_data1 [-overlap:,:] += my_data2[:overlap,:]
                        my_data1 [-overlap :,:] /=2
                        my_data1 = np.append(my_data1, my_data2[overlap:,:], axis=0)
                    
                        
                        
#                        with open(image_path_a +style+'_'+motion+'_'+ str(sequence)+'_'+str(clip)  + '_input_epoch0'+'.csv', 'r') as csvfile:
#                            
#                            # creating a csv reader object 
#                            csvreader= csv.reader(csvfile) 
#                            for row in  csvreader:
                        clip +=1
                        if clip<10:
                            clip_str = '0'+str(clip)
                        else:
                            clip_str = str(clip)
    
                    np.savetxt(output_path, my_data1, delimiter=",")
                        
                
                    sequence +=1
            
      
#        with open(output_path, 'w' ) as csv_file:




            
if __name__ == '__main__':
    concatenate()

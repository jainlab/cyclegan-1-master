"""
This script serves to convert frame motions within a csv to bvh. The skeleton
currently is being populated from a bvh, but should eventually be replaced with
information for a generic child/adult. Lines marked with '# TEMP' represent the
logic to do this - as it is a temporary solution until the generic skeletons
are made.
"""
import sys
import csv
import os

CSV_PATH =  r'D:\mocap_adult2child\csv\\'
BVH_INPUT_PATH = r'D:\mocap_adult2child\bvh_3dof\\' # TEMP
OUTPUT_FOLDER = r'D:\mocap_adult2child\output_bvh\\'

FRAME_TIME = 0.00833333 # 120fps

"""
This function populates the motion data within @csvFile to @bvhFile. It assumes
@bvhFile already has skeleton data.

@param bvhFile: the bvh file to write to
@param csvFile: the csv file to retrieve motion data from
"""
def writeFromCsv(bvhFile, csvFile):
    csvReader = csv.reader(csvFile, delimiter=',')
    next(csvReader, None)  # skip the headers
    rows = list(csvReader) # Used since frame amount must be known before looping
    frameCnt = len(rows)

    bvhFile.write('MOTION\nFrames: ' + str(frameCnt) + '\nFrame Time: ' + str(FRAME_TIME) + '\n')
    
    for i, line in enumerate(rows):
        # Used for if file has 6dof and only either pos or rot is needed
        # pos = [line[i] for i in range(0, len(line)) if i%6 == 0 or (i - 1)%6 == 0 or (i - 2)%6 == 0]
        # rot = [line[i] for i in range(0, len(line)) if (i + 3)%6 == 0 or (i + 2)%6 == 0 or (i + 1)%6 == 0]

        bvhFile.write(" ".join(line) + '\n')

"""
This function copies the skeleton data (hierarchy info, offsets, channels,
etc.) from @oldBvh to @newBvh.

@param newBvh: the bvh file to copy the data to
@param oldBvh: the bvh file to copy the data from
"""
def writeFromBvh(newBvh, oldBvh):
    for line in oldBvh: # Write beginning hierarchy info
        if 'MOTION' in line: # Next lines are motion data -> break
            break
            
        
        newBvh.write(line)

            
def iterateDir(root):
    outputSub = None
    
    # Make output dirs
    os.makedirs(OUTPUT_FOLDER, exist_ok=True)
    os.makedirs(OUTPUT_FOLDER + 'adult\\', exist_ok=True)
    os.makedirs(OUTPUT_FOLDER + 'child\\', exist_ok=True)
    
    for path, dirs, files in os.walk(root): 
        output_sub = 'adult\\' if '\\adult' in path.lower() else 'child\\'
        csv_files = [f for f in files if ('.csv' in f)] # only care about bvh files
       
        # Loop through all bvh files and create csv files
        for csv_file in  csv_files:
            csv_name, csv_ext = os.path.splitext(csv_file)
           
            csv_input_path =  path + "\\" + csv_file
            bvh_input_path = BVH_INPUT_PATH + output_sub + csv_name + '.bvh' # TEMP
            bvh_output_path = OUTPUT_FOLDER + output_sub + csv_name + '.bvh'
            
            # Open files
            csv_input = open(csv_input_path, 'r') # Will throw error if file DNE
            bvh_input = open(bvh_input_path, 'r') # Will throw error if file DNE
            bvh_output = open(bvh_output_path, 'w') # Will override if file exists # TEMP

            # Write new file
            writeFromBvh(bvh_output, bvh_input) # TEMP
            writeFromCsv(bvh_output, csv_input)

            # Close files
            bvh_output.close()
            bvh_input.close()
            csv_input.close()                  

if __name__ == '__main__':
    iterateDir(CSV_PATH)

import tensorflow as tf
import numpy as np
from . import cyclegan_datasets
from . import model


def _load_samples(csv_name, image_type):
    filename_queue = tf.train.string_input_producer(
        [csv_name])

    reader = tf.TextLineReader()
    _, csv_filename = reader.read(filename_queue)

    record_defaults =[tf.constant([], dtype=tf.string)]
    record_defaults.extend([tf.constant([], dtype=tf.float64)]*model.IMG_HEIGHT*model.IMG_WIDTH*model.IMG_CHANNELS)
    record_defaults.append(tf.constant([], dtype=tf.string))
    record_defaults.extend([tf.constant([], dtype=tf.float64)]*model.IMG_HEIGHT*model.IMG_WIDTH*model.IMG_CHANNELS)

    
    
    input= list(tf.decode_csv(
        csv_filename, record_defaults=record_defaults))
    
#    filename_i, filename_j = tf.decode_csv(
#        csv_filename, record_defaults=record_defaults)
#
#    file_contents_i = tf.read_file(filename_i)
#    file_contents_j = tf.read_file(filename_j)
    if image_type == '.csv':
        
        filename_i = input[0]
        image_a = input[1: 1+ model.IMG_WIDTH*model.IMG_HEIGHT*model.IMG_CHANNELS]
        filename_j = input[1+ model.IMG_WIDTH*model.IMG_HEIGHT*model.IMG_CHANNELS]
        image_b = input[2+ model.IMG_WIDTH*model.IMG_HEIGHT*model.IMG_CHANNELS:]

        
#        image_decoded_A = tf.image.decode_jpeg(
#            file_contents_i, channels=model.IMG_CHANNELS)
#        image_decoded_B = tf.image.decode_jpeg(
#            file_contents_j, channels=model.IMG_CHANNELS)

        image_decoded_b = tf.reshape(
        image_b,
            [model.IMG_WIDTH,model.IMG_HEIGHT, model.IMG_CHANNELS ],
            name=None
        )
#        image_decoded_b = tf.reshape(
#        image_b,
#            [model.IMG_WIDTH, model.IMG_HEIGHT,1 ],
#            name=None
#        )
#        image_decoded_b = tf.transpose (image_decoded_b, perm=[1, 2, 0])
##   
##
#        image_decoded_a = tf.reshape(
#            image_a,
#            [model.IMG_WIDTH, model.IMG_HEIGHT,1 ],
#            name=None
#        )
        image_decoded_a = tf.reshape(
            image_a,
           [model.IMG_WIDTH,model.IMG_HEIGHT, model.IMG_CHANNELS ],
            name=None
        )
#        image_decoded_a = tf.transpose (image_decoded_a, perm=[1, 2, 0])

   
    return image_decoded_a, image_decoded_b ,  filename_i,  filename_j


def load_data(dataset_name):
    """

    :param dataset_name: The name of the dataset.
    :param image_size_before_crop: Resize to this size before random cropping.
    :param do_shuffle: Shuffle switch.
    :param do_flipping: Flip switch.
    :return:
    """
    if dataset_name not in cyclegan_datasets.DATASET_TO_SIZES:
        raise ValueError('split name %s was not recognized.'
                         % dataset_name)

    csv_name = cyclegan_datasets.PATH_TO_CSV[dataset_name]

    image_i, image_j ,filename_i,  filename_j= _load_samples(
        csv_name, cyclegan_datasets.DATASET_TO_IMAGETYPE[dataset_name])



#
#    image_i = tf.subtract(tf.div(image_i, 127.5), 1)
#    image_j = tf.subtract(tf.div(image_j, 127.5), 1)

    
    image_i_max= tf.reduce_max(image_i, axis=[1], keep_dims=True)
    image_i_min= tf.reduce_min(image_i, axis=[1], keep_dims=True)
        
    image_j_max= tf.reduce_max(image_j, axis=[1], keep_dims=True)
    image_j_min= tf.reduce_min(image_j, axis=[1], keep_dims=True)
        
#        image_decoded_b_mean, image_decoded_b_sd = tf.nn.moments(image_decoded_b, axes=[1], keep_dims=True)
        

##        
#    image_i =tf.div(tf.subtract(2*image_i, (image_i_min + image_i_max )), (image_i_max-image_i_min))
#    image_j =tf.div(tf.subtract(2*image_j, (image_j_min + image_j_max )), (image_j_max-image_j_min))
#    
##    
##    
#    
    
    # Batch
    do_shuffle = False
    if do_shuffle is True:
        images_i, images_j ,images_i_max,images_i_min, images_j_max,images_j_min , filenames_i, filenames_j= tf.train.shuffle_batch(
            [image_i, image_j, image_i_max,image_i_min, image_j_max,image_j_min , filename_i, filename_j], 1, 500, 10)
    else:
        images_i, images_j, images_i_max, images_i_min, images_j_max, images_j_min, filenames_i, filenames_j= tf.train.batch(
            [image_i, image_j,image_i_max,image_i_min, image_j_max,image_j_min , filename_i, filename_j], 1)

  
    inputs = {
        'images_i': images_i,
        'images_j': images_j,
        'images_i_max': images_i_max,
        'images_j_max': images_j_max,
        'images_i_min': images_i_min,
        'images_j_min': images_j_min,
        'images_i_name': filenames_i,
        'images_j_name': filenames_j,
    }
        
    return inputs

def main():
    
        init = (tf.global_variables_initializer(),
                tf.local_variables_initializer())
      

      
        with tf.Session() as sess:
            sess.run(init)

            # Restore the model to run the model from last checkpoint
            
            self_inputs= load_data("horse2zebra_train")
            coord = tf.train.Coordinator()
            threads = tf.train.start_queue_runners(coord=coord)
            print("after threads")
            for epoch in range(0,20):
                print(str(epoch))
      
                
                inputs = sess.run(self_inputs)


#                images_i_max =   inputs['images_i_max']  # type: rrr
#                images_j_max =  inputs['images_j_max']
#                images_i_min    =     inputs['images_i_min']
#                images_j_min  =  inputs['images_j_min']
#                images_i = inputs['images_i']
#                images_j = inputs['images_j']

                images_j_name = inputs['images_j_name']
                images_i_name = inputs['images_i_name']
                
                print(images_i_name)
                print(images_j_name)
#                
            coord.request_stop()
            coord.join(threads)

    
    
if __name__ == '__main__':
    main()   




import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
import pandas as pd
import os
import click
#  Parameters

# Filter requirements.
order = 2       # order of butterworth filter (2)
fs = 12.0       # sample rate, Hz
cutoff = 5      # desired cutoff frequency of the filter, Hz (3)

# First make some data to be filtered.
T = 1.0          # seconds
n = int(T * fs)  # total number of samples

t = np.linspace(0, T, n, endpoint=False)

# Functions


def butter_lowpass(cutoff, fs, order=5):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    b, a = butter(order, normal_cutoff, btype='lowpass', analog=False)
    return b, a


def butter_lowpass_filter(data, cutoff, fs, order=5):
    b, a = butter_lowpass(cutoff, fs, order=order)
    y = filtfilt(b, a, data)
    return y


def process_file(file_root, file_name):
    # file_name = "childlike_fastwalking_1_1_input_epoch5.csv"
    # file_name = input("Press enter a file name (within the current directory): ")

    file_path = os.path.join(file_root, file_name)
    df = pd.read_csv(file_path, header=None)

    df_out = pd.DataFrame()
#    print(df.size())
    # update sample rate to match number of data points in the current column of the csv file
    fs = len(df[0])
    print(len(df))
    print(len(df.columns))
    for i in range(len(df.columns)):
#        print(i)
#        print(df)
        data = df[i]
        # print(data)

        

        # Filter the data, and plot both the original and filtered signals.
        y = butter_lowpass_filter(data, cutoff, fs, order)

        # output filtered data to csv file

        df_tmp = pd.DataFrame(data=y)
        df_out = pd.concat([df_out, df_tmp], axis=1)

        # Plotting code for debugging

        # plt.plot(t, data, 'b-', label='data')
        # plt.plot(t, y, 'g-', linewidth=2, label='filtered data')
        # plt.grid()
        # plt.legend()
        #
        # plt.subplots_adjust(hspace=0.35)
        # plt.show()

        # input("Press enter to continue.")

        # END plotting code

    out_directory = file_root + '/smoothed/'
    if not os.path.exists(out_directory):
        os.makedirs(out_directory)

    # df_out = df_out.iloc[1:]
    df_out.to_csv(out_directory + file_name, index=False, header=False)

@click.command()    
@click.option('--dir_name',
              type=click.STRING,
              default=None,
              help='Input data.')


def main(dir_name):
  
    for file in os.listdir(dir_name):
        if file.endswith(".csv"):
            print("processing file: " + os.path.join(dir_name, file))
            process_file(dir_name, file)  
    
    
    

if __name__ == '__main__':
    main()

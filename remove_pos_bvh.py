"""
This script removes all positional data from a bvh file, keeping only
rotational data. The root retains all 6 DOF, while the rest of the hierarchy
keep only rotation.

The file is modified in place. A .bak file is created during execution, in case
the script errors out. If this happens, you must replace the bvh file with the
.bak contents. If a file is successfully modified, the .bak is deleted.

It works with multiple files provided as separate CL arguments.
"""

import sys # Using for stdout
import os # Using to get extension of file
import fileinput # Using to modify file in place

SKEL_KEYWORD = 'CHANNELS'
MOT_KEYWORD = 'Frame Time:'

"""
This file iterates over the bvh file and removes the positional data.

@param file bvhFile: the bvh file to modify
"""
def removePos(bvhFile):
    # Iterate until after the root -> keep all
    for line in bvhFile:
        sys.stdout.write(line)

        if SKEL_KEYWORD in line: # This line is root -> break
            break

    # Iterate beginning hierarchy info -> keep rot only
    for line in bvhFile:
        if SKEL_KEYWORD in line: # Only modify CHANNELS line
            lst = line.split(' ')
            endOfKeyword = [i for i, s in enumerate(lst) if SKEL_KEYWORD in s][0] # Done to keep correct indentation since splitting on whitespace
            line = (' '.join(lst[:endOfKeyword + 1] + ['3'] + lst[-3:]))

        sys.stdout.write(line)

        if MOT_KEYWORD in line: # Next lines are motion data -> break
            break

    # Iterate frame data -> keep all root and only rot after
    for line in bvhFile:
        nums = line.split(' ') # List of numbers only
        line = nums[:6] + [nums[i] for i in range(6, len(nums)) if (i + 3)%6 == 0 or (i + 2)%6 == 0 or (i + 1)%6 == 0]

        sys.stdout.write(' '.join(line) + '\n') # DO NOT remove newline at the end -> prevents corruption of file

if __name__ == '__main__':
    if (len(sys.argv) < 2):
        print('Error: too few arguments specified')

    for i in range(1, len(sys.argv)):
        fileName, fileType = os.path.splitext(sys.argv[i])

        # Check file type
        if (fileType.lower() != '.bvh'):
            print('Error: unsupported file type')

        # Reading file in this way reads in whitespace
        bvhFile = fileinput.input(files=sys.argv[i], inplace=True, backup='.bak')

        # Rewrite bvh
        removePos(bvhFile)
        bvhFile.close()

        # Delete backup (done after in case of interrupt)
        os.remove(sys.argv[i] + '.bak')

import cv2
import os
import sys

from cv2 import VideoWriter, VideoWriter_fourcc

"""
This script is used to create a .mov from a directory of images. The directory
is passed as a CL argument. It assumes the directory only contains images,
images are ordered alphabetically, and every image should be included. The
video name is named after the first image (minus the extensions). The default
fps is 30.

ex syntax) python vid_from_imgs.py path/to/img/dir
"""

fps = 120
vid_ext = '.mov'

if __name__ == '__main__':
    if (len(sys.argv) < 2):
        print("Error: too few arguments specified")

    img_dir = sys.argv[1]

    # Get all images in the directory
    imgs = os.listdir(img_dir)

    # Extract file name (to be used as video name)
    vid_name = imgs[0].split(os.extsep, 1)[0] + vid_ext

    # Get dimensions of first img
    frame = cv2.imread(os.path.join(img_dir, imgs[0]))
    height, width, layers = frame.shape

    # Create video
    video = cv2.VideoWriter(vid_name, VideoWriter_fourcc(*'mp4v'), fps, (width, height))

    # Write images to video
    for img in imgs:
        video.write(cv2.imread(os.path.join(img_dir, img)))

    # Clean up
    cv2.destroyAllWindows()
    video.release()
